<?php

/**
 * @file
 * Definition of views_handler_filter_node_type.
 */

/**
 * Filter by node type.
 *
 * @ingroup views_filter_handlers
 */
class socialfeed_views_handler_filter_service extends views_handler_filter_in_operator {
  function get_value_options() {
    if (!isset($this->value_options)) {
      $this->value_title = t('Social Feed Services');
      $services = socialfeed_get_services();
      $options = array();
      foreach ($services as $service => $info) {
        $options[$service] = t($info->name);
      }
      asort($options);
      $this->value_options = $options;
    }
  }
}
class socialfeed_views_handler_filter_status extends views_handler_filter_in_operator {
  function get_value_options() {
    $options[1] = 'Published';
    $options[0] = 'Un-Published';
    $this->value_options = $options;
  }
}

