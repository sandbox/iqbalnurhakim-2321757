<?php

/**
 * @file
 * Socialfeed of views_default objects.
 */

/**
 * Implements hook_views_default_views().
 */
function socialfeed_views_default_views() {
  $view = new view();
  $view->name = 'social_feed_contents';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'socialfeed';
  $view->human_name = 'Social Feed Contents';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Social Feed Contents';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'administer socialfeed';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '50';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'sfid' => 'sfid',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'sfid' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Field: Socialfeed: Socialfeed ID */
  $handler->display->display_options['fields']['sfid']['id'] = 'sfid';
  $handler->display->display_options['fields']['sfid']['table'] = 'socialfeed';
  $handler->display->display_options['fields']['sfid']['field'] = 'sfid';
  /* Field: Socialfeed: Created Time */
  $handler->display->display_options['fields']['created']['id'] = 'created';
  $handler->display->display_options['fields']['created']['table'] = 'socialfeed';
  $handler->display->display_options['fields']['created']['field'] = 'created';
  $handler->display->display_options['fields']['created']['date_format'] = 'long';
  $handler->display->display_options['fields']['created']['second_date_format'] = 'long';
  /* Field: Socialfeed: Fetched Time */
  $handler->display->display_options['fields']['inserted']['id'] = 'inserted';
  $handler->display->display_options['fields']['inserted']['table'] = 'socialfeed';
  $handler->display->display_options['fields']['inserted']['field'] = 'inserted';
  $handler->display->display_options['fields']['inserted']['date_format'] = 'long';
  $handler->display->display_options['fields']['inserted']['second_date_format'] = 'long';
  /* Field: Socialfeed: Socialfeed Content */
  $handler->display->display_options['fields']['content']['id'] = 'content';
  $handler->display->display_options['fields']['content']['table'] = 'socialfeed';
  $handler->display->display_options['fields']['content']['field'] = 'content';
  /* Field: Socialfeed: Socialfeed Status */
  $handler->display->display_options['fields']['status']['id'] = 'status';
  $handler->display->display_options['fields']['status']['table'] = 'socialfeed';
  $handler->display->display_options['fields']['status']['field'] = 'status';
  /* Field: Socialfeed: Socialfeed Username */
  $handler->display->display_options['fields']['user_name']['id'] = 'user_name';
  $handler->display->display_options['fields']['user_name']['table'] = 'socialfeed';
  $handler->display->display_options['fields']['user_name']['field'] = 'user_name';
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = 'Operation';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '<a href="/admin/config/services/socialfeed/contents/publish/[sfid]?destination=admin/config/services/socialfeed/contents">Publish/Unpublish</a>';
  /* Filter criterion: Socialfeed: Socialfeed Service */
  $handler->display->display_options['filters']['service']['id'] = 'service';
  $handler->display->display_options['filters']['service']['table'] = 'socialfeed';
  $handler->display->display_options['filters']['service']['field'] = 'service';
  $handler->display->display_options['filters']['service']['exposed'] = TRUE;
  $handler->display->display_options['filters']['service']['expose']['operator_id'] = 'service_op';
  $handler->display->display_options['filters']['service']['expose']['label'] = 'Socialfeed Service';
  $handler->display->display_options['filters']['service']['expose']['operator'] = 'service_op';
  $handler->display->display_options['filters']['service']['expose']['identifier'] = 'service';
  $handler->display->display_options['filters']['service']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    30037204 => 0,
    103270763 => 0,
    175031666 => 0,
    197817655 => 0,
  );
  /* Filter criterion: Socialfeed: Socialfeed Status */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'socialfeed';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['exposed'] = TRUE;
  $handler->display->display_options['filters']['status']['expose']['operator_id'] = 'status_op';
  $handler->display->display_options['filters']['status']['expose']['label'] = 'Socialfeed Status';
  $handler->display->display_options['filters']['status']['expose']['operator'] = 'status_op';
  $handler->display->display_options['filters']['status']['expose']['identifier'] = 'status';
  $handler->display->display_options['filters']['status']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    30037204 => 0,
    103270763 => 0,
    175031666 => 0,
    197817655 => 0,
  );
  /* Filter criterion: Socialfeed: Socialfeed Content */
  $handler->display->display_options['filters']['content']['id'] = 'content';
  $handler->display->display_options['filters']['content']['table'] = 'socialfeed';
  $handler->display->display_options['filters']['content']['field'] = 'content';
  $handler->display->display_options['filters']['content']['operator'] = 'word';
  $handler->display->display_options['filters']['content']['exposed'] = TRUE;
  $handler->display->display_options['filters']['content']['expose']['operator_id'] = 'content_op';
  $handler->display->display_options['filters']['content']['expose']['label'] = 'Socialfeed Content';
  $handler->display->display_options['filters']['content']['expose']['operator'] = 'content_op';
  $handler->display->display_options['filters']['content']['expose']['identifier'] = 'content';
  $handler->display->display_options['filters']['content']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    30037204 => 0,
    103270763 => 0,
    175031666 => 0,
    197817655 => 0,
  );
  /* Filter criterion: Socialfeed: Socialfeed Username */
  $handler->display->display_options['filters']['user_name']['id'] = 'user_name';
  $handler->display->display_options['filters']['user_name']['table'] = 'socialfeed';
  $handler->display->display_options['filters']['user_name']['field'] = 'user_name';
  $handler->display->display_options['filters']['user_name']['operator'] = 'word';
  $handler->display->display_options['filters']['user_name']['exposed'] = TRUE;
  $handler->display->display_options['filters']['user_name']['expose']['operator_id'] = 'user_name_op';
  $handler->display->display_options['filters']['user_name']['expose']['label'] = 'Socialfeed Username';
  $handler->display->display_options['filters']['user_name']['expose']['operator'] = 'user_name_op';
  $handler->display->display_options['filters']['user_name']['expose']['identifier'] = 'user_name';
  $handler->display->display_options['filters']['user_name']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    30037204 => 0,
    103270763 => 0,
    175031666 => 0,
    197817655 => 0,
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'admin/content/socialfeed';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Social Feeds';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $views['socialfeed_content'] = $view;

$view = new view();
$view->name = 'socialfeed_example';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'socialfeed';
$view->human_name = 'SocialFeed Example';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'SocialFeed Example';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'none';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '10';
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['row_plugin'] = 'fields';
/* Field: Socialfeed: Image */
$handler->display->display_options['fields']['attachment']['id'] = 'attachment';
$handler->display->display_options['fields']['attachment']['table'] = 'socialfeed';
$handler->display->display_options['fields']['attachment']['field'] = 'attachment';
$handler->display->display_options['fields']['attachment']['label'] = '';
$handler->display->display_options['fields']['attachment']['element_label_colon'] = FALSE;
/* Field: Socialfeed: Socialfeed Content */
$handler->display->display_options['fields']['content']['id'] = 'content';
$handler->display->display_options['fields']['content']['table'] = 'socialfeed';
$handler->display->display_options['fields']['content']['field'] = 'content';
/* Field: Socialfeed: Socialfeed Service */
$handler->display->display_options['fields']['service']['id'] = 'service';
$handler->display->display_options['fields']['service']['table'] = 'socialfeed';
$handler->display->display_options['fields']['service']['field'] = 'service';
$handler->display->display_options['fields']['service']['label'] = '';
$handler->display->display_options['fields']['service']['element_label_colon'] = FALSE;
/* Field: Socialfeed: User Image */
$handler->display->display_options['fields']['user_picture']['id'] = 'user_picture';
$handler->display->display_options['fields']['user_picture']['table'] = 'socialfeed';
$handler->display->display_options['fields']['user_picture']['field'] = 'user_picture';
$handler->display->display_options['fields']['user_picture']['label'] = '';
$handler->display->display_options['fields']['user_picture']['element_label_colon'] = FALSE;
/* Field: Socialfeed: Socialfeed Username */
$handler->display->display_options['fields']['user_name']['id'] = 'user_name';
$handler->display->display_options['fields']['user_name']['table'] = 'socialfeed';
$handler->display->display_options['fields']['user_name']['field'] = 'user_name';
$handler->display->display_options['fields']['user_name']['label'] = 'Posted By: ';
$handler->display->display_options['fields']['user_name']['element_label_colon'] = FALSE;

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['path'] = 'socialfeed';

  
  $views['socialfeed_example'] = $view;
  return $views;
}
