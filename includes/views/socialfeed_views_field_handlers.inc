<?php

/**
 * @file
 * Provide views data and handlers for socialfeed.module
 */

/**
 * Field handler to provide simple renderer social media image into drupal image cache and add link.
 *
 * @ingroup views_field_handlers
 */
class socialfeed_views_handler_field_image extends views_handler_field {

  function init(&$view, &$options) {
    parent::init($view, $options);
    // Don't add the additional fields to groupby
    if (!empty($this->options['link_to_content'])) {
      if ($this->field == 'attachment') {
        $this->additional_fields['link_to_content'] = array('table' => 'socialfeed', 'field' => 'link_to_content');
      }
      else if ($this->field == 'user_picture') {
        $this->additional_fields['link_to_user_profile'] = array('table' => 'socialfeed', 'field' => 'link_to_user_profile');
      }
    }
  }
  function render($values) {
    $value = $values->{$this->field_alias};
    $info = image_get_info($value);
    if (strlen($value)) {
      $output = array(
        '#theme' => 'imagecache_external',
        '#path' => $value,
        '#style_name' => $this->options['image_style'],
        '#alt' => 'Social Media Image',
      );
    }
    if ($this->options['link_to_content']) {
      if ($this->field_alias == 'socialfeed_user_picture') {
        $path = $values->socialfeed_link_to_user_profile;
      }
      else {
        $path = $values->socialfeed_link_to_content;
      }
      $output = array(
        '#theme' => 'link',
        '#text' => drupal_render($output),
        '#path' => $path,
        '#options' => array(
          'attributes' => array('target' => '_blank'),
          'html' => TRUE,
        ),
      );
    }
    return $output;
  }

  function option_definition() {
    $options = parent::option_definition();
    $options['image_style'] = array('default' => 'medium');
    $options['link_to_content'] = array('default' => TRUE);
    return $options;
  }

  /**
   * Provide image style option
   */
  function options_form(&$form, &$form_state) {
    $image_styles = image_styles();
    foreach ($image_styles as $name => $image_style) {
      $options[$name] = $image_style['label'];
    }
    $form['image_style'] = array(
      '#title' => t('Select Image Style'),
      '#description' => t("Select Image Style."),
      '#type' => 'select',
      '#options' => $options,
      '#default_value' => $this->options['image_style'],
    );

    $form['link_to_content'] = array(
      '#title' => t('Link this field to the original piece of content'),
      '#description' => t("Enable to override this field's links."),
      '#type' => 'checkbox',
      '#default_value' => !empty($this->options['link_to_content']),
    );
    parent::options_form($form, $form_state);
  }
}

/**
 * 
 */
class socialfeed_views_handler_field_status extends views_handler_field {
  function render($values) {
    if ($values->{$this->field_alias} == 1) {
      return 'Published';
    }
    return 'Un-Published';
  }

}
