<?php
/**
 * @file
 * Provide views data and handlers for socialfeed.module
 */

/**
 * Implements hook_views_data().
 */
function socialfeed_views_data() {
  $data['socialfeed']['table']['group'] = t('Socialfeed');
  $data['socialfeed']['table']['base'] = array(
    'field' => 'sfid',
    'title' => 'Socialfeed',
    'help' => 'Socialfeed ID',
    'wieght' => 10
  );

  $data['socialfeed']['sfid'] = array(
    'title' => 'Socialfeed ID',
    'help' => 'Socialfeed Internal ID',
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'hanlder' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );

  $data['socialfeed']['sfeid'] = array(
    'title' => 'Socialfeed External ID',
    'help' => 'socialfeed External ID',
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'hanlder' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numberic ',
    ),
  );

  $data['socialfeed']['service'] = array(
    'title' => t('Socialfeed Service'),
    'help' => t('Socialfeed service'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'socialfeed_views_handler_filter_service',
    ),
    'sort' => array(
      'hanlder' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  $data['socialfeed']['content'] = array(
    'title' => t('Socialfeed Content'),
    'help' => t('Social feed content'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'hanlder' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  
  $data['socialfeed']['status'] = array(
    'title' => t('Socialfeed Status'),
    'help' => t('Socialfeed published status'),
    'field' => array(
      'handler' => 'socialfeed_views_handler_field_status',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'socialfeed_views_handler_filter_status',
    ),
    'sort' => array(
      'hanlder' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_filter_numberic',
    ),
  );

  $data['socialfeed']['created'] = array(
    'title' => t('Created Time'),
    'help' => t('The time this content was posted on social media.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  $data['socialfeed']['inserted'] = array(
    'title' => t('Fetched Time'),
    'help' => t('The time this content was fetched to the database.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  $data['socialfeed']['attachment'] = array(
    'title' => t('Image'),
    'help' => t('Attached Image to socialfeed post'),
    'field' => array(
      'handler' => 'socialfeed_views_handler_field_image',
      'click sortable' => TRUE,
    ),
  );

  $data['socialfeed']['user_id'] = array(
    'title' => t('Socialfeed user id'),
    'help' => t('Socialfeed user id'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'hanlder' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numberic',
    ),
  );

  $data['socialfeed']['user_name'] = array(
    'title' => t('Socialfeed Username'),
    'help' => t('Socialfeed Username'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'hanlder' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  $data['socialfeed']['user_picture'] = array(
    'title' => t('User Image'),
    'help' => t('Social feed user picture'),
    'field' => array(
      'handler' => 'socialfeed_views_handler_field_image',
      'click sortable' => TRUE,
    ),
  );

  return $data;
}