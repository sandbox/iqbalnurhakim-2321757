<?php

/**
 * @file
 * Provide admin functionality for socialfeed module.
 */

/**
 * Menu callback; Provide the administration settings page.
 */
function socialfeed_settings() {
  $form = array();

  $form['description'] = array(
    '#markup' => t('Social feed module general configurations.'),
  );

  $form['socialfeed_update_time'] = array(
    '#type' => 'select',
    '#title' => 'Update time',
    '#options' => array(
      'hourly' => t('Hourly'),
      'daily' => t('Daily'),
      'weekly' => t('Weekly'),
      'monthly' => t('Monthly'),
    ),
    '#description' => t('Update time periode.'),
    '#default_value' => variable_get('socialfeed_update_time', 'daily'),
  );

  $form['socialfeed_published_default'] = array(
    '#type' => 'checkbox',
    '#title' => 'Published by default',
    '#description' => t('If checked all imported items will be set to published.'),
    '#default_value' => variable_get('socialfeed_published_default', 1),
  );

  $form = system_settings_form($form);
  $form['#submit'][] = 'socialfeed_set_job';

  return $form;
}

/**
 * Form Callback: Setting job scheduler.
 */
function socialfeed_set_job($form, $form_state) {
  if (module_exists('job_scheduler')) {
    $values = $form_state['values'];
    $time = 3600;
    $update_time = $values['socialfeed_update_time'];
    switch ($update_time) {
      case 'daily':
        $time = $time * 24;
      break;
      case 'weekly':
        $time = $time * 24 * 7;
      break;
      case 'monthly':
        $time = $time * 24 * 30;
      break;
    }

    $job = array(
      'type' => 'socialfeed',
      'period' => $time,
      'periodic' => TRUE,
    );

    JobScheduler::get('socialfeed_import')->set($job);
  }
}

/**
 * Options for social feed item live time.
 * @return array 
 */
function socialfeed_items_livetime_options() {
  return array(
    'remove-1-year' => 'Remove 1 year old item',
    'remove-6-months' => 'Remove 6 months old item',
    'remove-3-months' => 'Remove 3 months old item',
    'remove-1-month' => 'Remove 1 month old item',
    'only-keep-1000' => 'Only keep 1000 items',
    'only-keep-500' => 'Only keep 500 items',
    'only-keep-100' => 'Only keep 100 items',
    'keep-all' => 'Keep all items forever',
  );
}

/**
 * Menu callback; Provide the administration overview page.
 */
function socialfeed_services() {
  $services = socialfeed_get_services();
  $header =  array( t('Name'), t('Configure'));
  $rows = array();
  foreach ($services as $service) {
    // Render name column.
    
    $row = array(theme('socialfeed_admin_overview', array('service' => $service)));
    
    // Set configure column.
    $cfg_url = 'admin/config/services/socialfeed/services/' . $service->service;
    $row[] = array('data' => l(t('Configure'), $cfg_url));
    $rows[] = $row;
  }
  $build['socialfeed_table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('No services available'),
  );
  
  return $build;
}

/**
 * Menu callback; Provide the services settings.
 */
function socialfeed_services_settings($form, &$form_state, $service) {
  $service = socialfeed_get_services($service);

  $form_state['service'] = $service;
  $form['#id'] = 'socialfeed-services-settings--' . $service->service;

  $form['description'] = array(
    '#markup' => $service->description,
  );

  $form['api_settings'] = array(
    '#type' => 'fieldset',
    '#title' => 'API settings',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $configs = $service->configs;
  foreach ($configs as $id => $config) {
    $element_id = $service->service . '_' . $id;
    $form['api_settings'][$element_id] = $config;
  }

  $form['query_settings'] = array(
    '#type' => 'fieldset',
    '#title' => 'Query Settings',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $query_settings = $service->query_settings;
  foreach ($query_settings as $query) {
    $element_id = $service->service . '_' . $query;
    $form['query_settings'][$element_id] = socialfeed_query_settings($query);
  }
  $form['query_settings']['add_more'] = array(
    '#type' => 'submit',
    '#value' => t('Add More Parameter'),
    '#submit' => array('socialfeed_add_param'),
    '#validate' =>  array('socialfeed_add_param_validate'),
    '#ajax' => array(
      'callback' => 'socialfeed_add_param_callback',
      'wrapper' => 'added-params'
    ),
  );
  
  $params = socialfeed_get_queries_table($service);

  $form['query_settings']['added_params'] = array(
    '#markup' => '<div id="added-params">' . $params . '</div>',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Configuration'),
  );

  $form['reset'] = array(
    '#type' => 'submit',
    '#value' => t('Reset Configuration'),
    '#submit' => array('socialfeed_services_settings_reset'),
  );
  return $form;
}

/**
 * Form Validation: validate the query parameters form.
 */
function socialfeed_add_param_validate($form, $form_state) {
  $service = $form_state['service'];
  $input = $form_state['values'];
  $query = array();
  foreach ($service->query_settings as $setting) {
    $element_id = $service->service . '_' . $setting;
    if (strlen($input[$element_id])) {
      $query[$setting] = $input[$element_id];
    }
  }
  if (!count($query)) {
    form_set_error('query_settings', t('You have to enter at least one parameter.'));  
  }
}

/**
 * Form callback: Submit callback of adding query parameters.
 */
function socialfeed_add_param($form, $form_state) {
  $service = $form_state['service'];
  $input = $form_state['values'];
  foreach ($service->query_settings as $setting) {
    $element_id = $service->service . '_' . $setting;
    $query[$setting] = $input[$element_id];
  }

  db_insert('socialfeed_queries')
    ->fields(array(
      'query' => serialize($query),
      'service' => $service->service,
    ))->execute();
  
}

/**
 * Ajax callback: adding query parameters.
 */
function socialfeed_add_param_callback($form, $form_state) {
  $service = $form_state['service'];
  return socialfeed_get_queries_table($service);
}

/**
 * Get table of queries.
 * @param object $service
 *   Social media service object.
 * @return String
 *   Html of queries table.
 */
function socialfeed_get_queries_table($service) {
  
  $results = socialfeed_get_queries($service);

  foreach ($service->query_settings as $setting) {
    $header[] = t(ucwords($setting));
  }
  $header[] = t('Delete');
  $rows = array();
  foreach ($results as $item) {
    $row = array();
    $params = unserialize($item->query);
    foreach ($params as $param) {
      $row[] = $param;
    }
    $delete_url = 'admin/config/services/socialfeed/delete-query/'
      . $item->service
      . '/' . $item->qid;
    
    $row[] = array(
      'data' => array(
        '#type' => 'link',
        '#title' => t('delete'),
        '#href' => $delete_url,
        '#ajax' => array(
          'wrapper' => 'added-params',
          'method' => 'json',
        ),
      ),
    );
    $rows[] = $row;
  }
  
  $build = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('No parameters available'),
  );
  return '<div id="added-params">' . render($build) . '</div>';
}

/**
 * Menu callback: Perform ajax process to delete query in database.
 * @param type $service
 *   Service Name.
 * @param type $qid
 *   Query Id.
 */
function socialfeed_delete_query($service, $qid) {
  $service_object = socialfeed_get_services($service);
  db_delete('socialfeed_queries')->condition('qid', $qid)->execute();  
  $html = socialfeed_get_queries_table($service_object);
  $commands[] = ajax_command_replace('#added-params', $html);
  $page = array('#type' => 'ajax', '#commands' => $commands);
  ajax_deliver($page);
}

/**
 * Form submit Callback: see @socialfeed_services_settings().
 */
function socialfeed_services_settings_submit($form, $form_state) {
  $values = $form_state['values'];
  $service = $form_state['service'];
  $configs = array();
  foreach ($service->configs as $id => $config) {
    $element_id = $service->service . '_' . $id;
    $configs[$id]= array(
        '#default_value' => $values[$element_id],
    ) + $config;
  }

  $info = (object) array(
    'service' => $service->service,
    'name' => $service->name,
    'description' => $service->description,
    'configs' => $configs,
    'modified' => 1
  );
  socialfeed_service_save($info);
}

/**
 * Form submit Callback: see @socialfeed_services_settings().
 */
function socialfeed_services_settings_reset($form, $form_state) {
  db_update('socialfeed_services')
    ->fields(array('modified' => 0))
    ->condition('service', $form_state['service']->service)
    ->execute();
 
  socialfeed_service_cache_reset();
  socialfeed_services_rebuild();
}

/**
 * Returns HTML for a social service description for the admin overview page.
 *
 * @param $variables
 *   An associative array containing:
 *   - service: An object containing 'name', 'service' (machine name) and 
 *     'description' of the service.
 *
 * @ingroup themeable
 */
function theme_socialfeed_admin_overview($variables) {
  $service = $variables['service'];
  $output = check_plain($service->name);
  $output .= ' <small>' . t('(Machine name: @type)', array('@type' => $service->service)) . '</small>';
  $output .= '<div class="description">' . filter_xss_admin($service->description) . '</div>';
  return $output;
}

/**
 * Generate queries form settings.
 * 
 * @param string $query
 *   Query key, for example: keywords, users.
 * @return array
 *   Form elements array.
 */
function socialfeed_query_settings($query) {
  switch ($query) {
    case 'keywords':
      $form = array(
        '#type' => 'textfield',
        '#title' => t('Keyword'),
        '#description' => t('Enter keyword'),
      );
    break;
    case 'users':
      $form = array(
        '#type' => 'textfield',
        '#title' => t('User'),
        '#description' => t('Enter User ID'),
      );
    break;
  }
  return $form;
}