WHAT IS SOCIALFEED
==================
This module provide ability to pull contents from multiple sources of social media services for example Facebook, Twitter, Instagram, etc

With this module you can manage multiple Socialfeed in one admin page, you can also moderate the post from socialmedia to show in your website.

CURRENT FEATURES
==================
* Support multiple services such as; facebook, twitter, instagram
* Publish/Unpublish posts
* Views Integration
* Scheduled fetching

Requirements
* Job Scheduler - https://www.drupal.org/project/job_scheduler

Supported By
* Bullseye Digital Agency - http://bullseye-digital.com
