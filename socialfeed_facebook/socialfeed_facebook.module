<?php

/* 
 * @file
 * Provide Facebook feed functionality.
 */

define('SOCIALFEED_FB_AUTH_URI', 'admin/config/services/socialfeed/facebook/auth');

/**
 * Implements hook_menu().
 */
function socialfeed_facebook_menu() {
  $items = array();
  $items[SOCIALFEED_FB_AUTH_URI] = array(
    'title' => 'Ajax callback',
    'page callback' => 'socialfeed_facebook_auth',
    'access arguments' => array('administer socialfeed'),
  );

  $items['admin/config/services/socialfeed/facebook/search-user/%'] = array(
    'title' => 'Ajax callback',
    'page callback' => 'socialfeed_facebook_search_user',
    'page arguments' => array(6),
    'access arguments' => array('administer socialfeed'),
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Implements hook_socialfeed_service_info().
 */
function socialfeed_facebook_socialfeed_service_info() {
  return array(
    'facebook' => array(
      'name' => t('Facebook'),
      'description' => t('Provide ability to pull post from facebook.'),
      'configs' => array(
        'app_id' => array(
          '#title' => t('APP ID'),
          '#type' => 'textfield',
          '#default_value' => '',
        ),
        'app_secret' => array(
          '#title' => t('APP secret'),
          '#type' => 'textfield',
          '#default_value' => '',
        ),
        'app_token' => array(
          '#title' => t('Access Token'),
          '#type' => 'textarea',
          '#default_value' => '',
        ),
      ),
      'query_settings' => array('keywords', 'users'), // Available queries;keywords,users
    ),
  );
}

/**
 * Implements hook_socialfeed_fetch().
 */
function socialfeed_facebook_socialfeed_fetch() {
  // Get service object.
  $service = socialfeed_get_services('facebook');
  // Retrieve queries params.
  $queries = socialfeed_get_queries($service);
  // FB instance.
  $fb = socialfeed_facebook_instance();

  $filtered = array();
  $non_filtered = array();

  foreach ($queries as $query) {
    $q = unserialize($query->query);

    // Skip if no users, facebook api v2 doesn't allow to search post only.
    if (!isset($q['users']) || !strlen($q['users'])) {
      continue;
    }

    // Extract users params.
    $user = explode(' - ', $q['users']);
    $user_id = reset($user);

    // Fetching the posts.
    $request_url = '/' . $user_id . '/feed?limit=250&fields=message,from,picture,full_picture,link,id';
    $status = $fb->request($request_url)->getGraphObject()->asArray();

    // Fetching the prof pic.
    $prof_pic_url = '/' . $user_id . '/picture?redirect=false';
    $prof_pic = $fb->request($prof_pic_url)->getGraphObject()->asArray();

    $prof_pics[$user_id] = $prof_pic->url;

    foreach ($status['data'] as $item) {
      // Filtering by keywords.
      if (isset($item->message)) {
        if (strlen($q['keywords']) && strpos($item->message, $q['keywords']) !== FALSE) {
          $filtered[] = $item;
        }
        else {
          $non_filtered[] = $item;
        }
      }
    }
  }
  $items = $non_filtered;
  if (!empty($filtered)) {
    $items = $filtered;
  }
  foreach ($items as $item) {
    $link = isset($item->link) ? $item->link : 'http://facebook.com/'.$item->id;
    $picture = isset($item->full_picture) ? $item->full_picture : '';
    $user_picture = isset($prof_pics[$item->from->id]) ? $prof_pics[$item->from->id] : '';
    $return[] = (object) array(
      'sfeid' => $item->id,
      'service' => 'facebook',
      'content' => $item->message,
      'status' => 1,
      'attachment' => $picture,
      'user_id' => $item->from->id,
      'link_to_content' => $link,
      'link_to_user_profile' => 'http://facebook.com/'.$item->from->id,
      'user_name' => $item->from->name,
      'user_picture' => $user_picture,
      'created' => strtotime($item->created_time),
      'inserted' => time(),
    );
  }
  return $return;
}

/**
 * Implements hook_form_alter().
 */
function socialfeed_facebook_form_alter(&$form, &$form_state, $form_id) {
  $socialfeed_id = 'socialfeed-services-settings--facebook';

  if ($form_id == 'socialfeed_services_settings' && $form['#id'] == $socialfeed_id) {
    $service = socialfeed_get_services('facebook');
    if (!strlen($service->configs['app_token']['#default_value'])) {
      $message = t('Please authenticate the user to make this working.');
      drupal_set_message(check_plain($message), 'warning');

      $form['authenticate'] = array(
        '#type' => 'submit',
        '#submit' => array('socialfeed_facebook_authenticate'),
        '#validate' => array('socialfeed_facebook_authenticate_validate'),
        '#value' => t('Authenticate'),
      );
    }
    else {
      $form['authenticate'] = array(
        '#type' => 'submit',
        '#submit' => array('socialfeed_facebook_authenticate'),
        '#validate' => array('socialfeed_facebook_authenticate_validate'),
        '#value' => t('Re-Authenticate'),
      );
    }

    $path = 'admin/config/services/socialfeed/facebook/search-user';
    $form['query_settings']['facebook_users']['#autocomplete_path'] = $path;
    $form['query_settings']['add_more']['#validate'] = array('socialfeed_facebook_add_param_validate');
  }
}

/**
 * Form callback: Validate username.
 * @param type $form
 * @param type $form_state
 */
function socialfeed_facebook_add_param_validate($form, $form_state) {
  $input = $form_state['values'];
  if (!strlen($input['facebook_users'])) {
    form_set_error('facebook_users', t('User parameter is required.'));  
  }
}

/**
 * Form callback: authenticate facebook oauth.
 */
function socialfeed_facebook_authenticate($form, $form_state) {
  $input = $form_state['values'];
  $config = array(
    'app_id' => $input['facebook_app_id'],
    'app_secret' => $input['facebook_app_secret'],
    'redirect_uri' => url(SOCIALFEED_FB_AUTH_URI, array( 'absolute' => TRUE))
  );

  $fb = socialfeed_facebook_instance($config);
  $url = $fb->getLoginUrl();
  drupal_goto($url);
}

/**
 * Form callback: validate facebook oauth auth.
 */
function socialfeed_facebook_authenticate_validate($form, $form_state) {
  $input = $form_state['values'];
  $message = '';

  $element = 'facebook_app_id';
  if (!isset($input[$element]) && !strlen($input[$element])) {
    $message = t('Please make you enter the correct app ID');
    $element = 'facebook_app_id';
    from_set_error($element, $message);
  }

  $element = 'facebook_app_secret';
  if (!isset($input[$element]) && !strlen($input[$element])) {
    $message = t('Please make you enter the correct app Secret');
    from_set_error($element, $message);
  }
}

/**
 * Menu Callback: Facebook post auth process.
 */
function socialfeed_facebook_auth() {
  $fb = socialfeed_facebook_instance();
  $access_token = $fb->getSession()->getToken();

  $service = socialfeed_get_services('facebook');
  $service->configs['app_token']['#default_value'] = $access_token;

  $info = (object) array(
    'service' => $service->service,
    'name' => $service->name,
    'configs' => $service->configs,
    'description' => $service->description,
    'modified' => $service->modified,
    'query_settings' => $service->query_settings,
  );

  socialfeed_service_save($info);
  drupal_goto('admin/config/services/socialfeed/services/facebook');
}

/**
 * Initiate facebook class.
 * 
 * @param type $config
 *   An array containing:
 *   - app_id: (required) Facebook APP Id.
 *   - app_secret: (required) Facebook APP secret.
 *   - app_token: (optional depends on use) Client access token.
 *   - redirect_uri: (optional for Auth Only) Redirect URI.
 * @return \SocialfeedFacebook
 */
function socialfeed_facebook_instance($config = array()) {
  // Load from static if isset.
  $facebook = &drupal_static(__FUNCTION__);
  if(isset($facebook)) {
    return $facebook;
  }
  if (empty($config)) {
    $redirect_uri = url(SOCIALFEED_FB_AUTH_URI, array( 'absolute' => TRUE ));
    $service = socialfeed_get_services('facebook');
    $config = array(
      'app_id' => $service->configs['app_id']['#default_value'],
      'app_secret' => $service->configs['app_secret']['#default_value'],
      'app_token' => $service->configs['app_token']['#default_value'],
      'redirect_uri' => $redirect_uri,
    );
  }
  return new SocialfeedFacebook($config);
}

/**
 * Menu Callback: generate JSON object for user autocomplete.
 * @param string $username
 *   Users json.
 */
function socialfeed_facebook_search_user($username) {
  $matches = array();
  $matches += socialfeed_facebook_get_users($username);
  $matches += socialfeed_facebook_get_users($username, 'page');
  $matches += socialfeed_facebook_get_users($username, 'group');
  drupal_json_output($matches);
}

/**
 * Get users from facebook.
 * 
 * @param type $username
 *  facebook username to search.
 * @param type $type
 *  search type could be; user, page, group.
 * @return string
 */
function socialfeed_facebook_get_users($username, $type = 'user') {
  $fb = socialfeed_facebook_instance();

  $query_array = array(
    'q' => $username,
    'type' => $type,
    'limit' => 10,
  );

  $query_string = drupal_http_build_query($query_array);
  $search_url = '/search?' . $query_string;

  $result = $fb->request($search_url)
    ->getGraphObject()
    ->asArray();

  $matches = array ();
  foreach ($result['data'] as $item) {
    $item->id = array(
      $item->id,
      $item->name,
    );

    $key = implode(' - ', $item->id);

    $matches += array(
      $key => $item->name . ' - ' . $type,
    );
  }
  return $matches;
}
