<?php

/*
 * @file
 * Very basic wrapper of Facebook api V.4
 */

/**
 * Imports
 */
require_once( 'Facebook/FacebookSession.php' );
require_once( 'Facebook/FacebookRedirectLoginHelper.php' );
require_once( 'Facebook/FacebookRequest.php' );
require_once( 'Facebook/FacebookResponse.php' );
require_once( 'Facebook/FacebookSDKException.php' );
require_once( 'Facebook/FacebookRequestException.php' );
require_once( 'Facebook/FacebookAuthorizationException.php' );
require_once( 'Facebook/HttpClients/FacebookHttpable.php' );
require_once( 'Facebook/HttpClients/FacebookCurl.php' );
require_once( 'Facebook/HttpClients/FacebookCurlHttpClient.php' );
require_once( 'Facebook/GraphObject.php' );

use Facebook\FacebookSession;
use Facebook\FacebookRequest;
use Facebook\GraphUser;
use Facebook\FacebookRequestException;
use Facebook\FacebookRedirectLoginHelper;


class SocialfeedFacebook {
  /**
   *
   * @var string
   *    Facebook app ID
   */
  private $appId = NULL;
  /**
   *
   * @var string
   *    Facebook app secret 
   */
  private $appSecret = NULL;
  /**
   *
   * @var \Facebook\FacebookSession
   *    FacebookSession instance.. 
   */
  private $session = NULL;
  /**
   *
   * @var \Facebook\FacebookRedirectLoginHelper
   *    Redirect login instance. 
   */
  private $loginHelper = NULL;

  /**
   * Class constructor.
   * 
   * @param array $config
   *   Contains following parameters:
   *   - app_id: Facebook APP ID.
   *   - app_secret: Facebook APP secret.
   *   - app_token: Facebook Access token.
   *   - redirect_uri: Redirect URL after facebook login.
   */
  public function __construct($config){
    $this->appId = $config['app_id'];
    $this->appSecret = $config['app_secret'];
    
    FacebookSession::setDefaultApplication($this->appId, $this->appSecret);
    
    if (isset($config['app_token'])) {
      $this->session = $this->_session($config['app_token']);
    }
    
    if(isset($config['redirect_uri'])){
      $this->loginHelper = new FacebookRedirectLoginHelper($config['redirect_uri']);
    } else {
      $this->loginHelper = new FacebookRedirectLoginHelper();
    }
  }

  /**
   * Initiate facebook session.
   * 
   * @param string $access_token
   *   Facebook access toke.
   * 
   * @return \Facebook\FacebookSession
   *   FacebookSession instance.
   */
  private function _session($access_token){
    return new FacebookSession($access_token);
  }

  /**
   * Get facebook token.
   * 
   * @return string
   *  Facebook access token.
   */
  public function getToken(){
    return $this->session->getToken();
  }

  /**
   * Get Facebook login url
   * @return string 
   *  Facebook Login URL.
   */
  public function getLoginUrl(){
    return $this->loginHelper->getLoginUrl();
  }

  /**
   * Get session after login.
   * @return \Facebook\FacebookSession
   */
  public function getSession(){
    return $this->loginHelper->getSessionFromRedirect();
  }

  /**
   * Call facebook API.
   * @param type $method
   *   Facebook api method. for example : /me.
   * @return \Facebook\FacebookResponse
   *   JSON Object.
   */
  public function request($method){
    $request = new FacebookRequest($this->session, 'GET', $method );
    return $request->execute();
  }
}
