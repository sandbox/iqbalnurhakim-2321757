<?php

/* 
 * @file
 * Hooks provided by Socialfeed module.
 * 
 * Here is the list of hooks invoked:
 * hook_socialfeed_service_info()
 * hook_socialfeed_fetch()
 * hook_socialfeed_insert($item)
 * hook_socialfeed_update($item)
 * 
 */

/**
 * Define socialfeed services,
 * 
 * This hook allows a module to define one or more social feed services.
 * For example facebook, twitter, etc.
 * 
 * @return
 *   An array of information defining the module's services. the array contains
 *   a sub-array for each service, with machine-readable name as key.
 *   Each sub-array has up to 3 attributes:
 *   - name: (required) the human-readable name of service.
 *   - configs: (required) the configurations of service, this can be made to be
 *     configurable in backend.
 *   - description: (required)  a brief description of the services.
 */
function hook_socialfeed_service_info(){
  return array(
    'facebook' => array(
      'name' => t('Facebook'),
      'description' => t('provide ability to pull post from facebook.'),
      'configs' => array(
        'app_id' => array(
          '#title' => t('APP ID'),
          '#type' => 'textfield',
          '#default_value' => '',
        ),
        'app_secret' => array(
          '#title' => t('APP secret'),
          '#type' => 'textfield',
          '#default_value' => '',
        ),
        'app_token' => array(
          '#title' => t('Access Token'),
          '#type' => 'textfield',
          '#default_value' => '',
        ),
      ),
    ),
  );
}

/**
 * Respond to cron execution.
 *
 * This hook is invoked from socialfeed_cron().
 *
 * This is hook is used to fetch content from social media based on user
 * predefined queries on the backend.
 *
 *
 * @return $items
 *   Fetched social media items.
 */
function hook_socialfeed_fetch() {
  $items[] = array(
    'sfeid' => '',
    'service' => '',
    'content' => '',
    'status' => '',
    'attachment' => '',
    'user_id' => '',
    'user_name' => '',
    'user_picture' => '',
    'created' => '',
    'inserted' => '',
  );
  return $items;
}

/**
 * Act on a feed item being inserted or updated.
 *
 * This hook is invoked from socialfeed_save() before the feed item is saved to 
 * the database.
 *
 * @param $item
 *   The feed item that is being inserted or updated.
 *
 */
function hook_socialfeed_presave($item){
    $item->status = 0;
}

/**
 * Respond to creation of a new social feed items.
 *
 * This hook is invoked from socialfeed_save() after the feed item is inserted
 * into the database.
 *
 * @param $item
 *   The feed item that is being inserted or updated.
 *
 * @ingroup node_api_hooks
 */
function hook_socialfeed_insert($item){
  db_insert('mytable')
    ->fields(array(
      'nid' => $item->sfid,
      'extra' => $item->extra,
    ))
    ->execute();
}

/**
 * Respond to updates to social feed items.
 *
 * This hook is invoked from socialfeed_save() after the feed item is updated
 * in the database.
 *
 * @param $item
 *   The feed item that is being inserted or updated.
 *
 * @ingroup node_api_hooks
 */
function hook_socialfeed_update($item){
  db_update('mytable')
    ->fields(array('extra' => $item->extra))
    ->condition('nid', $item->sfid)
    ->execute();
}